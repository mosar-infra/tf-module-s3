# s3/outputs

output "bucket" {
  value = aws_s3_bucket.bucket
}

output "objects" {
  value = aws_s3_object.file
}
