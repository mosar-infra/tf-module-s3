# s3/variables.tf

variable "environment" {}
variable "bucket" {}
variable "acl" {}
variable "file_directory" {}
variable "managed_by" {}

