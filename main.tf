# s3/main.tf

resource "aws_s3_bucket" "bucket" {
  bucket = var.bucket
  tags = {
    Environment = var.environment
    ManagedBy = var.managed_by
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "sse" {
  bucket = var.bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm     = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_acl" "acl" {
  bucket = aws_s3_bucket.bucket.id
  acl    = var.acl
}

resource "aws_s3_object" "file" {
  for_each = var.file_directory != "" ? fileset(var.file_directory, "**"): []
  bucket = var.bucket
  key    = each.value
  source    = "${var.file_directory}/${each.value}"
  tags = {
    Environment = var.environment
    ManagedBy = var.managed_by
  }
}
